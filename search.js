window.addEventListener('keypress', function (event) {
  // console.log(event);

  // are we currently in some kind of text input?  skip it.
  if (event.target.nodeName == 'TEXTAREA'
      || event.target.nodeName == 'INPUT'
      || event.target.nodeName == 'SELECT'
      || event.target.nodeName == 'OPTION')
  {
    return;
  }

  // slash
  if (47 == event.keyCode) {
    var string = prompt('search:', window.getSelection());
    window.find(string);
  }
});
